import java.util.concurrent.TimeUnit

import cats.implicits._
import cats.effect._
import cats.effect.concurrent._

import scala.concurrent.duration.FiniteDuration
import scala.io.StdIn
import scala.util.Try

object Main extends IOApp {

  type RunningTask = Fiber[IO, Unit]

  case class TaskDescriptor(name: String, duration: FiniteDuration)

  sealed trait Command

  case class Start(task: TaskDescriptor) extends Command
  case class InvalidInput(input: String) extends Command
  object PrintInfo extends Command
  object Quit extends Command

  def run(args: List[String]): IO[ExitCode] = for {
    state <- Ref.of[IO, Map[String, RunningTask]](Map())
    exCode <- cliLoop(state)
  } yield exCode

  def cliLoop(state: Ref[IO, Map[String, RunningTask]]): IO[ExitCode] = for {
    cmd       <- cliCommand
    continue  <- processCommand(cmd, state)
    ret       <- if (continue) cliLoop(state) else IO.pure(ExitCode.Success)
  } yield ret

  def processCommand(evt: Command, state: Ref[IO, Map[String, RunningTask]]): IO[Boolean] = evt match {

    case Start(task) => for {
      _       <- IO { println(s"Scheduling task ${task.name}") }
      fiber   <- IO.shift >> execute(task, state).start
      _       <- state.modify(st => (st.updated(task.name, fiber), ()))
    } yield true

    case Quit => for {
      _             <- IO { println("quitting...") }
      runningTasks  <- state.modify(st => (Map(), st.values.toList))
      _             <- runningTasks.map(_.cancel).sequence
    } yield false

    case InvalidInput(input) => IO { println("Invalid input") } >> IO.pure(true)

    case PrintInfo =>
      val print = state.get.map(_.keys.toSeq) >>= printTasks
      print >> IO.pure(true)
  }

  def printTasks(tasks: Seq[String]): IO[Unit] = IO {
    println("Running:")
    tasks.foreach(println)
  }

  def cliCommand: IO[Command] = for {
    _ <- IO { println("enter new command:") }
    line <- IO { StdIn.readLine }
    cmd = line match {
      case "quit" => Quit
      case "info" => PrintInfo
      case _ =>
        Try[Command] {
          val parts = line.split(' ')
          val duration = FiniteDuration(parts(1).toLong, TimeUnit.SECONDS)
          val task = TaskDescriptor(parts(0), duration)
          Start(task)
        }
        .getOrElse(InvalidInput(line))
    }
  } yield cmd

  def execute(task: TaskDescriptor, state: Ref[IO, Map[String, RunningTask]]): IO[Unit] = {
    val io = for {
      _ <- IO { println(s"starting task ${task.name}") }
      _ <- IO.sleep(task.duration)
      _ <- IO { println(s"ended task ${task.name}") }
    } yield ()

    io.guarantee(
      state.modify(running => (running - task.name, ()))
    )
  }
}
